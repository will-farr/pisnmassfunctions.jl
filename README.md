Module implementing a parameterized model for the binary black hole mass
function with a simplified treatment of the effect of the pair instability.
