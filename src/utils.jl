"""
    cumtrapz(xs, ys)

Return the cumulative trapezoidal estimate of the integral of `ys` over `xs`.

Unlike the `scipy` default, the returned array will be of the same shape as the
inputs; the first element will always be `zero(ys[1])`.
"""
function cumtrapz(xs, ys)
    out = similar(ys)
    out[1] = zero(ys[1])
    for i in 2:length(out)
        out[i] = out[i-1] + 0.5*(xs[i]-xs[i-1])*(ys[i]+ys[i-1])
    end
    out
end

"""
    interp(xs, ys)

Return a function that linearly interpolates a `y(x)` between the given `xs` and
`ys`.

`y(x)` is constant outside the range of the `xs`.  `xs` must be given in
increasing order or the results will be undefined.
"""
function interp(xs, ys)
    function f_interp(x)
        i = searchsortedfirst(xs, x)
        if i == 1
            return ys[1]
        elseif i == length(ys)+1
            return ys[end]
        else
            r = (x - xs[i-1]) / (xs[i]-xs[i-1])
            return r*ys[i] + (1-r)*ys[i-1]
        end
    end

    f_interp
end

"""    with_seed(f, seed)

Perform the zero-argument function `f` after seeding the default RNG with `seed`.

Ensures that the RNG is re-seeded randomly when `f` exits normally or abnormally
(i.e. by throwing an exception).

Note the useful syntax:

    with_seed(1234) do
        ...
    end
"""
function with_seed(f, seed)
    Random.seed!(seed)
    try
        f()
    finally
        Random.seed!()
    end
end

"""
    effective_sample_number(weights)

Returns the effective number of samples from an array of importance weights.
"""
function effective_sample_number(weights)
    s = sum(weights)

    s*s / sum(weights.*weights)
end

"""
    draw_sample_index(wts[, size])

Return an index or indices into the array of `wts` drawn with probability
proportional to `wts`.
"""
function draw_sample_index(wts)
    cwts = cumsum(wts)
    r = rand()*cwts[end]
    searchsortedfirst(cwts, r)
end

function draw_sample_index(wts, size)
    cwts = cumsum(wts)
    [searchsortedfirst(cwts, rand()*cwts[end]) for i in 1:size]
end