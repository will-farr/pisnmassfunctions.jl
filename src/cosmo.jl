"""
    E(z, ΩM, w)

The inverse of the integrand of the comoving distance integral:

``E(z) = \\sqrt{\\Omega_M \\left(1 + z \\right)^3 + \\Omega_k \\left( 1 + z \\right)^2 + \\Omega_\\Lambda \\left( 1 + z \\right)^{3\\left( 1 + w\\right)}}``
"""
function E(z, ΩM, w)
    opz = 1 + z
    opz2 = opz*opz
    opz3 = opz2*opz
    sqrt(ΩM*opz3 + (1-ΩM)*opz3^(1+w))
end

"""
    dCs(zs, ΩM, w)

Returns the comoving distance (in units of the Hubble distance) at the given
redshifts.

The array of redshifts is used for a trapezoid rule approximation to the
integral, so it must be dense enough to provide sufficient accuracy.
"""
function dCs(zs, ΩM, w)
    cumtrapz(zs, 1 ./ E.(zs, ΩM, w))
end

"""
    dAs(zs, dCs)

Returns the angular diameter distance at the given redshift and transverse
comoving distances.
"""
function dAs(zs, dCs)
    return dCs ./ (1 .+ zs)
end

"""
    dLs(zs, dCs)

Returns the luminosity distances at the given redshifts and transverse comoving
distances.
"""
function dLs(zs, dCs)
    return dCs .* (1 .+ zs)
end

"""
    ddLsdzs(zs, dLs, ΩM, w)

Returns the derivative of the luminosity distance with redshift.
"""
function ddLsdzs(zs, dLs, ΩM, w)
    dLs ./ (1 .+ zs) .+ (1 .+ zs)./E.(zs, ΩM, w)
end

"""
    comoving_volume_element(zs, dCs, ΩM, w)

Returns the prefactor ``A`` in ``\\mathrm{d}V = A \\mathrm{d}Ω \\mathrm{d}z``.
"""
function comoving_volume_element(zs, dCs, ΩM, w)
    dCs .* dCs ./ E.(zs, ΩM, w)
end

"""
    dH(h)

Returns the Hubble distance in Gpc from `h = H_0 / (100 km/s/Mpc)`.
"""
function dH(h)
    2.99792 / h
end

"""
    h_default

Dimensionless Hubble constant (`H0 / 100 km/s/Mpc`) from Planck 2018.
"""
const h_default = 0.6737

"""
    ΩM_default

Dimensionless matter density (relative to the critical density) from Planck
2018.
"""
const ΩM_default = 0.3147

"""
    w_default

Cosmological constant (`w = -1`).
"""
const w_default = -1.0

const _coef = let
    zs = expm1.(log(1.0):0.01:log(1.0+10.0))[2:end]
    N = length(zs)
    c = cosmology(h = h_default, OmegaM=ΩM_default, OmegaK=0.0, w0=w_default)
    dV = [ustrip(UnitfulAstro.Gpc^3, 4*pi*Cosmology.comoving_volume_element(c, zz)) for zz in zs]
    M = hcat(ones(N), zs, zs.^2, zs.^3, zs.^4, zs.^5, zs.^6)
    b = zs.^2 ./ dV
    coef = M \ b

    coef
end

"""
    dVdz(z)

The comoving volume element at redshift `z` (including the 4*pi angular factor)
from the Planck 2018 cosmology.  Units are ``\\mathrm{Gpc}^3``.  The
approximation is produced from a rational function fit that is good to
substantially better than 1% for ``0 < z < 4``.
"""
function dVdz_default(z)
    denom = zero(z)
    for i in size(_coef, 1):-1:1
        c = _coef[i]
        denom = c + z*denom
    end
    z*z/denom
end

const cosmology_default = cosmology(h = h_default, OmegaM = ΩM_default, OmegaK = 0, w0 = w_default, wa = 0)
