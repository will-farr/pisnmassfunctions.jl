"""
    mref

When a reference mass is needed (e.g. for normalization), this is it.
"""
const mref = 30.0

"""
    zref

When a reference redshift is needed (e.g. for normalization) this is it.
"""
const zref = 0.0

"""
    log_logistic_unit(x)

The log of the logistic function but scaled to have unit derivative at `x = 0`.

``\\frac{1}{1 + \\exp\\left( -4 x \\right)}``

but computed in a way that does not under/overflow for large/small `x`.
"""
function log_logistic_unit(x)
    if x > 0
        -log1p(exp(-4*x))
    else
        4*x - log1p(exp(4*x))
    end
end

"""
    log_dNdmCO(m, a, b)

The log of the mass function for the primary CO core.

A broken power law, with exponent `-a` for ``m \\ll 20 \\, M_\\odot`` and
exponent `-b` for ``m \\gg 20 \\, M_\\odot``, transitioning smoothly over a mass
scale of ~10% of the break mass (so ``\\sim 2.0 \\, M_\\odot``).
"""
function log_dNdmCO(m, a, b)
    mtr = 20
    x = 10 * (m - mtr)/mtr

    l1 = -a*log(m/mtr) + log_logistic_unit(-x)
    l2 = -b*log(m/mtr) + log_logistic_unit(x)

    logaddexp(l1, l2) - log(mtr)
end

"""
    _a(mPISN, mBH_max)

Gives the curvature (quadratic coefficient in CO mass) of the core-mass to
bh-mass relation.
"""
function _a(mPISN, mBH_max)
    1.0/(4.0*(mBH_max - mPISN))
end

"""
    mCO_max(mPISN, mBH_max)

The CO core mass corresponding to the remnant mass `mBH_max`.
"""
function mCO_max(mPISN, mBH_max)
    2.0*mBH_max - mPISN
end

"""
    max_CO_mass(mPISN, mBH_max)

The maximum CO core mass above which the remnant mass is zero.
"""
function max_CO_mass(mPISN, mBH_max)
    a = _a(mPISN, mBH_max)
    mcm = mCO_max(mPISN, mBH_max)

    sqrt(mBH_max/a) + mcm
end

"""
    mean_mBH_of_mCO(mCO, mPISN, mBH_max)

The (mean) BH mass as a function of the CO core mass.
"""
function mean_mBH_of_mCO(mCO, mPISN, mBH_max)
    if mCO < mPISN
        mCO
    else
        a = _a(mPISN, mBH_max)
        mcm = mCO_max(mPISN, mBH_max)
        x = mCO - mcm
        max(mBH_max - a*x*x, zero(mCO))
    end
end

"""
    mCO_of_mean_mBH(mBH, mPISN, mBH_max)

The inverse of `mean_mBH_of_mCO`---the CO core mass(es) corresponding to the
given mean BH mass.

Because the CO mass to BH mass relation turns over, the inverse is
double-valued; this function returns two values, `(mCO_a, mCO_b)`.

For black hole masses above the maximum BH mass, returns the CO core mass
corresponding to the maximum BH mass `(mCO_max, mCO_max)`.
"""
function mCO_of_mean_mBH(mBH, mPISN, mBH_max)
    mCOm = mCO_max(mPISN, mBH_max)
    a = _a(mPISN, mBH_max)

    if mBH < mPISN
        d = sqrt((mBH_max - mBH)/a)
        mBH, (mCOm + d)
    elseif mBH < mBH_max
        d = sqrt((mBH_max - mBH)/a)
        (mCOm - d), (mCOm + d)
    else
        mCOm, mCOm
    end
end

@doc raw"""
    log_dNdmBH_pisn(mBH, a, b, mPISN, mBH_max, sigma)

An analytic approximation to the stellar-origin BH mass function.

The mass function incorporates a toy model of PISN physics: the black hole mass
at fixed CO core mass is assumed to follow a Gaussian distribution with standard
deviation ``\sigma`` and mean 
```math
m_{\mathrm{BH},\mathrm{mean}} = \begin{cases}
    m_{\mathrm{CO}} & m_{\mathrm{BH},\mathrm{mean}} < m_\mathrm{PISN} \\
    m_{\mathrm{BH},\mathrm{max}} - a \left( m_\mathrm{CO} - m_{\mathrm{CO}, \mathrm{max}} \right)^2 & \mathrm{otherwise}
\end{cases}.
```
This function models the onset of pulsational mass loss at ``m_\mathrm{BH} =
m_\mathrm{PISN}`` with a maximum BH mass of ``m_{\mathrm{BH},\mathrm{max}}``.
The parameter ``a`` is chosen to make the mean BH mass smooth at ``m_\mathrm{BH}
= m_\mathrm{PISN}`` (see the `_a` function in this module).

The CO core mass function is assumed to follow a broken power law with exponent
``-a`` at low mass and ``-b`` above ``20 \, M_\odot``.  Integrating over the CO
core mass at fixed BH mass gives the mass function, which will have a PISN
pileup between ``m_\mathrm{PISN}`` and ``m_{\mathrm{BH},\mathrm{max}}`` followed
by a Gaussian falloff above ``m_{\mathrm{BH},\mathrm{max}}``.

The function returned here is normalized by the condition that the CO core mass
function ``m \mathrm{d} N / \mathrm{d} m`` is `1` at ``m_\mathrm{CO} = 20 \,
M_\odot``.

When ``\sigma \gtrsim m_{\mathrm{BH},\mathrm{max}} - m_\mathrm{PISN}`` the
analytic approximation will be poor; it also breaks down in the limit
``m_\mathrm{BH} \to 0`` where the curvature scale of the CO mass function
becomes comparable to ``\sigma``.
"""
function log_dNdmBH_pisn(mBH, a, b, mPISN, mBH_max, sigma)
    beta = _a(mPISN, mBH_max)

    alpha = mBH - mBH_max

    log_prefactor = -log(2*sigma) + 0.5*log(abs(alpha)/beta)

    z = alpha*alpha / (4*sigma*sigma)

    mCOm, mCOp = mCO_of_mean_mBH(mBH, mPISN, mBH_max)
    log_dNm = log_dNdmCO(mCOm, a, b)
    log_dNp = log_dNdmCO(mCOp, a, b)

    if mBH < mBH_max
        log_I = log_prefactor + 0.5*log(pi/2) + log(besselix(-1/4, z) + besselix(1/4, z))
        if mBH < mPISN
            logaddexp(log_dNm, log_I + log_dNp - log(2))
        else
            log_I + logaddexp(log_dNm, log_dNp) - log(2)
        end
    elseif mBH == mBH_max
        # Handle degenerate case
        log_dNm + log(0.86003998732451953538) - 0.5*log(beta*sigma)
    else
        # Can use either log_dNm or log_dNp here, as they are the same for m > mBH_max
        log_dNm + log_prefactor - 0.5*log(pi) + log(besselkx(1/4, z)) - 2*z 
    end
end

@doc raw"""
    log_md_rate(z, lambda, zp, kappa)

Log of the (un-normalized) parameterized Madau-Dickinson form star formation
rate:

```math
\frac{\left( 1 + z \right)^\lambda}{1 + \left( \frac{1+z}{1+z_p} \right)^\kappa}
```
"""
function log_md_rate(z, lambda, zp, kappa)
    lambda*log1p(z) - log1p(((1+z)/(1+zp))^kappa)
end

@doc raw"""
    log_dNdm_highmass(m, c, mref)

A power-law combined with a logistic turnon at `mref`.

```math
\frac{\mathrm{d} N}{\mathrm{d} m} = \mathrm{logit}^{-1} \left( 100 \left( \frac{m}{m_\mathrm{ref}} \right)\right) \left( \frac{m}{m_\mathrm{ref}} \right)^{-c}
```
"""
function log_dNdm_highmass(m, c, mref)
    log_logistic_unit(100*(m/mref-1)) - c*log(m/mref)
end

@doc raw"""
    log_dNdm1dqdVdt(m1, q, z, a, b, c, beta, mPISN, mBH_max, sigma, fpl, lambda, zp, kappa)

The merger rate density for our full model.  

It is given by 
```math
\frac{\mathrm{d} N}{\mathrm{d} m_1 \mathrm{d} q \mathrm{d} V \mathrm{d} t} = m_1 f\left( m_1 \right) f\left( q m_1 \right) g(q) r(z).
```
The normalization is arbitrary.

The function ``f`` is a sum of the PISN mass function described above
(`log_dNdmBH_pisn`) and a high-mass power law (`log_dNdm_highmass`) whose
amplitude near `mBH_max` is `fpl` times the PISN mass function.

The function ``g`` is a "pairing function" that is proportional to the total
mass to the ``\beta``:
```
g(q) = \left( \frac{1 + q}{2} \right)^\beta
```

The function ``r`` is a Madau-Dickinson-like parameterized merger rate evolution
(`log_md_rate`)
```math
r(z) = \frac{\left( 1 + z \right)^\lambda}{1 + \left( \frac{1+z}{1+z_p} \right)^\kappa}.
```
"""
function log_dNdm1dqdVdt(m1, q, z, a, b, c, beta, mPISN, mBH_max, sigma, fpl, lambda, zp, kappa)
    m2 = q*m1

    log_fpl = log(fpl)
    log_f_bhmax = log_dNdmBH_pisn(mBH_max, a, b, mPISN, mBH_max, sigma)

    log_f1 = log_dNdmBH_pisn(m1, a, b, mPISN, mBH_max, sigma)
    log_f2 = log_dNdmBH_pisn(m2, a, b, mPISN, mBH_max, sigma)

    log_g = beta*(log1p(q) - log(2))

    log_r = log_md_rate(z, lambda, zp, kappa)

    log_1 = logaddexp(log_f1, log_f_bhmax + log_fpl + log_dNdm_highmass(m1, c, mBH_max))
    log_2 = logaddexp(log_f2, log_f_bhmax + log_fpl + log_dNdm_highmass(m2, c, mBH_max))

    log_1 + log_2 + log_g + log_r + log(m1)
end

