@testset "Random Cosmology Against Cosmology.jl" begin
    h = 0.35 + (1.4-0.35)*rand()
    ΩM = 0.1 + (0.5-0.1)*rand()
    w = -1.5 + (-0.5+1.5)*rand()

    c = cosmology(h=h, OmegaM=ΩM, w0=w, wa = 0.0, OmegaK=0.0)

    zs = expm1.(log(1.0):0.001:log(1.0+10.0))
    dC = dCs(zs, ΩM, w)
    dA = dAs(zs, dC)
    dL = dLs(zs, dC)
    ddLdz = ddLsdzs(zs, dL, ΩM, w)
    dVdz = PISNMassFunctions.comoving_volume_element(zs, dC, ΩM, w)

    dH_ = dH(h)

    dC = dC .* dH_
    dA = dA .* dH_
    dL = dL .* dH_
    ddLdz = ddLdz .* dH_
    dVdz = dVdz .* dH_ .* dH_ .* dH_

    z = 1.0 + (2.0-1.0)*rand()

    @test isapprox(ustrip(u"Gpc", hubble_dist(c, 0)), dH_,
                   rtol=1e-3, atol=1e-3)

    @test isapprox(ustrip(u"Gpc", comoving_radial_dist(c, z)),
                   interp(zs, dC)(z),
                   rtol=1e-3, atol=1e-3)

    @test isapprox(ustrip(u"Gpc", angular_diameter_dist(c, z)),
                   interp(zs, dA)(z),
                   rtol=1e-3, atol=1e-3)

    @test isapprox(ustrip(u"Gpc", luminosity_dist(c, z)),
                   interp(zs, dL)(z),
                   rtol=1e-3, atol=1e-3)

    d = diff(dL) ./ diff(zs)
    @test isapprox(0.5*(d[10] + d[11]),
                   ddLdz[11],
                   rtol=1e-3, atol=1e-3)

    @test isapprox(ustrip(u"Gpc"^3, Cosmology.comoving_volume_element(c, z)),
                   interp(zs, dVdz)(z),
                   rtol=1e-3, atol=1e-3)
end
