using Cosmology
using Distributions
using PISNMassFunctions
using Test
using Trapz
using Unitful
using UnitfulAstro

@testset "cosmo tests" begin
    include("cosmo-tests.jl")
end

@testset "model tests" begin
    include("model-tests.jl")
end
