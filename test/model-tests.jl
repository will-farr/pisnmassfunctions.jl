@testset "PISN stellar mass function" begin
    a = 2.35
    b = 1.35
    mPISN = 35.0
    mBH_max = 45.0
    sigma = 2.0

    mBHs = exp.(log(30.0):0.01:log(50.0))
    mCOs = exp.(log(5.0):0.001:log(max_CO_mass(mPISN, mBH_max)))

    log_dN = map(mBHs) do mBH
        dN = exp.(log_dNdmCO.(mCOs, a, b))
        wts = pdf.(Normal.(mean_mBH_of_mCO.(mCOs, mPISN, mBH_max), sigma), mBH)
        log(trapz(mCOs, dN .* wts))
    end

    log_dN_analytic = log_dNdmBH_pisn.(mBHs, a, b, mPISN, mBH_max, sigma)
    
    @test all(isapprox.(log_dN, log_dN_analytic, atol=2e-1))
end

# @testset "mass function normalization" begin
#     log_dNdtheta = PISNMassFunctions.log_dNdtheta_fn(2.0, 1.0, 0.0, 40.0, 45.0, 1.0, 0.1, 4.0, 0.0, 2.7, 1.9, 5.6, m_norm = 30.0, z_norm = 0.001)
#     @test isapprox(log_dNdtheta(30.0, 1.0, 0.001) + log(30.0), log(PISNMassFunctions.dVdz_default(0.001)) - log1p(0.001), rtol=1e-2, atol=1e-2)
# end

# @testset "stellar mass function" begin
#     for m1 in 20.0:5.0:55.0
#         for m2 in 20.0:5.0:m1
#             q = m2/m1
#             a = 2.35 + 0.1*randn()
#             b = 1.9 + 0.1*randn()
#             beta = 2.0 + 0.5*randn()
#             mPISN = 40.0 + rand()
#             mBH_max = 46.0 + rand()
#             sigma = 1.0 + rand()

#             log_dN_model = log_dNdm1dq_stellar(m1, q, a, b, beta, mPISN, mBH_max, sigma)
#             log_dN_approx = log_dNdm1dq_stellar_approx(m1, q, a, b, beta, mPISN, mBH_max, sigma)
#             @test isapprox(log_dN_model, log_dN_approx, atol=1e-1, rtol=0)
#         end
#     end
# end
